# Startup
- Make sure the current python environment has pipenv installed
- `python -m pipenv run jupyter lab`

# References From Lukas
https://pad.otc.coscine.dev/PwOqKIlyQgS1ynKiY_leIQ

https://git.rwth-aachen.de/matthias.fingerhuth/NFDI-Wikidata

https://jupyterhub.hpc.itc.rwth-aachen.de:9651/hub/login?next=%2Fhub%2Fhome
